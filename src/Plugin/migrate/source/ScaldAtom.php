<?php

namespace Drupal\migrate_source_scald\Plugin\migrate\source;

use Drupal\Core\Language\LanguageInterface;
use Drupal\migrate\Row;
use Drupal\migrate_drupal\Plugin\migrate\source\d7\FieldableEntity;

/**
 * Drupal 7 scald_atom source.
 *
 * Available configuration options:
 * - translations: (optional) [bool] If TRUE, migrate translations of atoms.
 * - atom_type: (optional) [string] Type of atoms to import.
 * - atom_provider: (optional) [string|array] Provider(s) of atoms to import
 *   (can be a string or an array of strings if several providers are wanted).
 *
 * Example with minimal options:
 * @code
 * source:
 *   plugin: migrate_source_scald_atom
 * @endcode
 *
 * Example with most options configured:
 * @code
 * source:
 *   plugin: migrate_source_scald_atom
 *   atom_type: video
 *   atom_provider:
 *     - scald_dailymotion
 *     - scald_vimeo
 *     - scald_youtube
 * @endcode
 *
 * @MigrateSource(
 *   id = "migrate_source_scald_atom",
 *   source_module = "scald",
 *   source_provider = "scald"
 * )
 */
class ScaldAtom extends FieldableEntity {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $fields = [
      'sid',
      'provider',
      'type',
      'base_id',
      'publisher',
      'actions',
      'title',
      'data',
      'created',
      'changed',
    ];
    if (empty($this->configuration['translations'])) {
      $fields[] = 'language';
    }
    $query = $this->select('scald_atoms', 's')
      ->fields('s', $fields);

    $query->condition('s.actions', 0, '>');

    if (isset($this->configuration['atom_type'])) {
      $query->condition('s.type', $this->configuration['atom_type']);
    }

    if (isset($this->configuration['atom_provider'])) {
      $query->condition(
        's.provider',
        $this->configuration['atom_provider'],
        is_array($this->configuration['atom_provider']) ? 'IN' : '='
      );
    }

    if (!empty($this->configuration['translations'])) {
      $alias = $query->leftJoin('entity_translation', 'et', "et.entity_type = 'scald_atom' AND et.entity_id=s.sid");
      $query->fields($alias, ['language']);
      $query->condition('et.language', LanguageInterface::LANGCODE_NOT_SPECIFIED, '<>');
      $query->condition('et.source', '', '<>');
    }

    $query->leftJoin(
      'field_data_scald_thumbnail',
      'thumbnail',
      "thumbnail.entity_type = 'scald_atom' AND thumbnail.entity_id = s.sid"
    );
    $query->fields('thumbnail', [
      'scald_thumbnail_fid',
      'scald_thumbnail_alt',
      'scald_thumbnail_title',
    ]);

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    $language = $row->getSourceProperty('language');
    $sid = $row->getSourceProperty('sid');

    // Get Field API field values.
    foreach ($this->getFields('scald_atom', $row->getSourceProperty('type')) as $field_name => $field) {
      // Ensure we're using the right language if the entity is translatable.
      $field_language = !empty($field['translatable']) ? $language : NULL;
      $row->setSourceProperty($field_name, $this->getFieldValues('scald_atom', $field_name, $sid, NULL, $field_language));
    }

    // If the node title was replaced by a real field using the Drupal 7 Title
    // module, use the field value instead of the node title.
    if ($this->moduleExists('title')) {
      $title_field = $row->getSourceProperty('title_field');
      if (isset($title_field[0]['value'])) {
        $row->setSourceProperty('title', $title_field[0]['value']);
      }
    }

    return parent::prepareRow($row);
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields = [
      'sid' => $this->t('Scald Atom ID'),
      'provider' => $this->t('Provider module name'),
      'type' => $this->t('Scald Atom type'),
      'base_id' => $this->t('Scald Atom base ID'),
      'publisher' => $this->t('Scald Atom publisher (User ID)'),
      'actions' => $this->t('Available Scald actions'),
      'title' => $this->t('Scald Atom title'),
      'data' => $this->t('Scald Atom data'),
      'created' => $this->t('Created timestamp'),
      'changed' => $this->t('modified timestamp'),
      'scald_thumbnail_fid' => $this->t('Thumbnail fid'),
      'scald_thumbnail_alt' => $this->t('Thumbnail alt'),
      'scald_thumbnail_title' => $this->t('Thumbnail title'),
    ];

    if (empty($this->configuration['translations'])) {
      $fields['language'] = $this->t('Scald Atom language');
    }

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    $ids['sid']['type'] = 'integer';
    $ids['sid']['alias'] = 's';

    if (!empty($this->configuration['translations'])) {
      $ids['language']['type'] = 'string';
      $ids['language']['alias'] = 'et';
    }

    return $ids;
  }

}
