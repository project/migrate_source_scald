<?php

namespace Drupal\migrate_source_scald\Plugin\migrate\process;

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\migrate\MigrateException;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\MigrateLookupInterface;
use Drupal\migrate\MigrateStubInterface;
use Drupal\migrate\Plugin\migrate\process\MigrationLookup;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Row;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Process wysiwyg strings containing scald dnd atom wrapper.
 *
 * Available configuration keys:
 * - migration_{type}: migration to map scald to media ids:
 *   - type: media bundle.
 * - context_to_view_mode: a list of scald context mapped to media view modes.
 *   context machine names will be mapped to same view mode key by default.
 *
 * Example:
 *
 * @code
 * process:
 *   new_text_field:
 *     plugin: transform_dnd_atom_to_media
 *     source: field_media_content
 *     migrations:
 *       audio: scald_atom_to_media_audio
 *       file: scald_atom_to_media_file
 *       video:
 *         - scald_atom_to_media_oembed
 *         - scald_atom_to_media_video
 *     context_to_view_mode:
 *       'big_old_display': 'desktop_view_mode'
 * @endcode
 *
 * This will replace scald attributes from the markup given in source field
 * (data-scald-*) to match the corresponding media embed integration.
 *
 * The div container will become a drupal-media tag.
 * The data-scald-align attribute will become data-align attribute.
 * The data-scald-sid will become data-entity-uuid.
 *
 * You need to enable the media embed plugin on /admin/config/content/formats.
 *
 * @MigrateProcessPlugin(
 *   id = "transform_dnd_atom_to_media",
 *   handle_multiples = TRUE
 * )
 */
class TransformDndAtomsToMedias extends MigrationLookup {

  /**
   * The entity_type.manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, ?MigrationInterface $migration = NULL) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $migration,
      $container->get('migrate.lookup'),
      $container->get('migrate.stub'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Constructs a TransformDndAtomsToMedias object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\migrate\Plugin\MigrationInterface $migration
   *   The Migration the plugin is being used in.
   * @param \Drupal\migrate\MigrateLookupInterface $migrate_lookup
   *   The migrate lookup service.
   * @param \Drupal\migrate\MigrateStubInterface $migrate_stub
   *   The migrate stub service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity_type.manager service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration, MigrateLookupInterface $migrate_lookup, MigrateStubInterface $migrate_stub, EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration, $migrate_lookup, $migrate_stub);
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {

    libxml_use_internal_errors(TRUE);

    if (is_null($value) || $value === '') {
      return $value;
    }

    if (mb_strpos($value, 'data-scald-sid') === FALSE) {
      return $value;
    }

    $dom = new \DOMDocument('1.0', 'utf-8');
    @$dom->loadHTML('<?xml version="1.0" encoding="utf-8"?>' . $value);

    $items = $dom->getElementsByTagName('div');

    // Get each atom wrappers data (sid, type, align, context):
    foreach ($items as $item) {
      $sid = $item->getAttribute('data-scald-sid');
      if ($sid) {
        $type = $item->getAttribute('data-scald-type');
        $align = $item->getAttribute('data-scald-align') ?? 'center';
        $context = $item->getAttribute('data-scald-context');
        $uuid = $this->sidToUuid($sid, $type);
        if (is_null($uuid)) {
          continue;
        }
        $drupalMedia = $dom->createElement('drupal-media', '');
        $drupalMedia->setAttribute('data-entity-type', 'media');
        $drupalMedia->setAttribute('data-entity-uuid', $uuid);
        $drupalMedia->setAttribute('data-align', $align);
        $imageStyle = $this->configuration['context_to_view_mode'][$context] ??
          $context;
        $drupalMedia->setAttribute('data-view-mode', $imageStyle);
        if ($item->firstElementChild instanceof \DOMElement && $item->firstElementChild->getAttribute('class') == 'dnd-caption-wrapper') {
          $allowedCaptionTags = [
            '<a>',
            '<em>',
            '<strong>',
            '<cite>',
            '<code>',
            '<br>',
          ];
          $caption = strip_tags($item->firstElementChild->ownerDocument->saveHtml($item->firstElementChild), $allowedCaptionTags);
          if (!empty($caption)) {
            $drupalMedia->setAttribute('data-caption', $caption);
          }
        }
        $replacements[] = [
          'source' => $item,
          'target' => $drupalMedia,
        ];
      }
    }
    if (empty($replacements)) {
      return $value;
    }
    foreach ($replacements as $data) {
      $data['source']->parentNode->replaceChild($data['target'], $data['source']);
    }
    $value = preg_replace('/(<\/?body>|<\/?html>)/', '', $dom->saveXML($dom->documentElement));
    return $value;
  }

  /**
   * Get media uuid corresponding to given scald atom sid.
   *
   * @param string $sid
   *   Atom ID.
   * @param string $mediaType
   *   Media bundle.
   *
   * @return string|null
   *   Media entity uuid.
   */
  protected function sidToUuid($sid, $mediaType) {

    if (empty($this->configuration['migrations'][$mediaType])) {
      return NULL;
    }
    // Re-throw any PluginException as a MigrateException so the executable
    // can shut down the migration.
    try {
      $migrations = $this->configuration['migrations'][$mediaType];
      if (!is_array($migrations)) {
        $migrations = [$migrations];
      }
      foreach ($migrations as $migration) {
        $results = $this->migrateLookup->lookup($migration, [$sid]);
        if (empty($results)) {
          continue;
        }
        $entityId = current($results)['mid'];
        $media = $this->entityTypeManager->getStorage('media')->load($entityId);
        if ($media) {
          return $media->uuid();
        }
      }
    }
    catch (PluginNotFoundException $e) {
      return NULL;
    }
    catch (MigrateException $e) {
      throw $e;
    }
    catch (\Exception $e) {
      throw new MigrateException(sprintf('A %s was thrown while processing this migration lookup', gettype($e)), $e->getCode(), $e);
    }
  }

}
