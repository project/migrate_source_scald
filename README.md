# Migrate source scald

This Drupal module provides a migrate source plugin to import Atoms from Drupal 7 Scald module.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/migrate_source_scald).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/migrate_source_scald).


## Requirements

Drupal core with the migrate module enabled.

Note: the source Drupal 7 site must use [scald module](https://www.drupal.org/project/scald)
to manage its medias. 


## Installation

Install as you would normally install a contributed Drupal module. Visit
https://www.drupal.org/node/1897420 for further information.


## Configuration

The source migrate plugin has these available configuration options:
- translations: (optional) [bool] If TRUE, migrate translations of atoms.
- atom_type: (optional) [string] Type of atoms to import.
- atom_provider: (optional) [string|array] Provider(s) of atoms to import
  (can be a string or an array of strings if several providers are wanted).
